import React from 'react'

const AsideIcon = ({title, image}) => {
    return (
        <div className={`aside__icon aside__icon--${title}`}>
            <img src={image} className={title} alt={title}/>
        </div>
    )
}

export default AsideIcon;
