import './Aside.css';
import dropshiplogo from '../../icons/dropship-logo.png';
import profileimg from '../../icons/profile-example.jpg';
// import dashboardlogo from '../../icons/dashboard.svg';
// import barslogo from '../../icons/bars.svg';
// import inventorylogo from '../../icons/inventory.svg';
// import cartlogo from '../../icons/cart.svg';
// import orderslogo from '../../icons/orders.svg';
// import transactionslogo from '../../icons/transactions.svg';
// import storeslogo from '../../icons/list.svg';
// import AsideIcon from './AsideIcon';

import { NavLink } from 'react-router-dom'

const icons = [
    {
        name: 'dashboard',
        class: "fas fa-tachometer-alt"
    },
    {
        name: 'catalog/all-categories',
        class: "fas fa-list-ul"
    },
    {
        name: 'inventory',
        class: "fas fa-boxes"
    },
    {
        name: 'cart',
        class: "fas fa-shopping-cart"
    },
    {
        name: 'orders',
        class: "fas fa-clipboard-check"
    },
    {
        name: 'transactions',
        class: "fas fa-exchange-alt"
    },
    {
        name: 'stores',
        class: "fas fa-clipboard-list"
    }
]

const Aside = () => {

    return (
        <aside className="aside">
            <section className="aside__item aside__item--nav">
                <div className="aside__icon aside__icon--dropship">
                    <img className="dropship__img" src={dropshiplogo} alt="dropship logo" />
                </div>
                <NavLink to="/profile">
                <div className="aside__icon aside__icon--user">
                    <img className="user__img" src={profileimg} alt="user logo" />
                </div>
                </NavLink>

                {icons.map(item => {
                    return <div className={`aside__icon aside__icon--${item.name}`} key={item.name}><NavLink to={`/${item.name}`} activeClassName='aside__icon--active'><i className={`icon ${item.class}`}></i></NavLink></div>
                })}

                {/* <AsideIcon title="dashboard" image={dashboardlogo}/>
                <AsideIcon title="bars" image={barslogo}/>
                <AsideIcon title="inventory" image={inventorylogo}/>
                <AsideIcon title="cart" image={cartlogo}/>
                <AsideIcon title="orders" image={orderslogo}/>
                <AsideIcon title="transactions" image={transactionslogo}/>
                <AsideIcon title="stores" image={storeslogo}/> */}
            </section>
        </aside>
    );
}

export default Aside;