import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import './ProductModification.css'
import { TextField } from '@material-ui/core'
import * as yup from 'yup';
import { useFormik } from 'formik';
import { getSingleProduct, removeProduct } from '../../Services/authorizationServices';

const idSchema = yup.object({
    id: yup
    .number()
    .integer('Price must be an integer')
    .required('Required')
})

export const ProductModification = ({title, value, icon}) => {

    const history = useHistory();

    const formik = useFormik({
        initialValues: {
            id: ''
        },
        onSubmit: (values) => {
            getSingleProduct(values.id).then(res => {
                if(value === "edit") {
                    history.push(`/inventory/${value}/${values.id}`);
                } 
                else if(value === "remove") {
                    removeProduct(values.id).then(res => {
                        alert("Product has been removed successfully");
                        localStorage.removeItem('products');
                    }).catch(err => {
                        alert("Could not remove product. Try again");
                    })
                }
            }).catch(err => {
                alert('Product id is incorrect');
            })
        },
        validationSchema: idSchema
    });

    const handleAddClick = () => {
        history.push(`/inventory/${value}`);
    }

    return (
        <div className="product-modification">
            <div className="product-modification__item product-modification__item--title">{title}</div>
            {value === "add" && <button className="product-modification__item product-modification__item--button" onClick={handleAddClick}>{icon}</button>}
            {(value === "edit" || value === "remove") &&
                <form onSubmit={formik.handleSubmit} className="product-modification__edit-container">
                    <TextField
                    placeholder="Product id"
                    variant="outlined"
                    name="id"
                    id="id"
                    type="number"
                    value={formik.values.id}
                    onChange={formik.handleChange}
                    onClick={(e) => e.stopPropagation()}
                    onBlur={formik.handleBlur}
                    error={formik.touched.id && Boolean(formik.errors.id)}
                    // helperText={formik.touched.id && formik.errors.id}
                    style={{width: "50%", color: "grey", margin: "12px 0px", backgroundColor: "white"}}
                    color="primary"
                    />
                    <button type="submit" className="product-modification__item product-modification__item--button product-modification__item--button-edit">{icon}</button>
                </form>
            }
        </div>
    )
}
