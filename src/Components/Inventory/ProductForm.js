import React, { useEffect, useState } from 'react'
import './ProductForm.css'
import TextField from '@material-ui/core/TextField';
import * as yup from 'yup';
import { addProduct, editProduct, getSingleProduct } from '../../Services/authorizationServices';
import { useFormik } from 'formik';
import { useHistory, useLocation } from 'react-router-dom';
import { InputAdornment } from '@material-ui/core';

const productSchema = yup.object({
    title: yup
    .string()
    .required('Required')
    .min(2, 'Title must exceed 2 characters'),
    description: yup
    .string()
    .required('Required')
    .min(4, 'Description must exceed 4 characters'),
    price: yup
    .number()
    .integer('Price must be an integer')
    .required('Required'),
    imageUrl: yup
    .string()
    .url('Input must be a url')
})

export const ProductForm = ({value, id}) => {

    const history = useHistory();
    const location = useLocation();

    useEffect(() => {
        console.log("location", location.pathname);
        if(location.pathname === "/inventory/edit") {
            history.push('/inventory');
        }
    }, [])

    useEffect(() => {
        if(id) {
            getSingleProduct(id).then(product => {
                formik.setValues(
                    {
                        title: product.title, 
                        description: product.description,
                        price: product.price,
                        imageUrl: product.imageUrl
                    })
            }).catch(err => {
                alert('Could not set initial values');
            })
        }
    }, [id])

    const formik = useFormik({
        initialValues: {
            title: '',
            description: '',
            price: '',
            imageUrl: ''
        },
        onSubmit: (values) => {
            handleProductSubmit(values);
        },
        validationSchema: productSchema
    });

    const handleProductSubmit = (values) => {
        if(id) {
            editProduct(id, values).then(res => {
                alert('Product edited successfully.');
                history.push('/inventory');
            }).catch(err => {
                alert('Could not update product. Try again.')
            })
        }
        else {
            addProduct(values).then(res => {
                alert('New product has been added successfully.')
            }).catch(err => {
                alert('Could not create new product. Try again.')
            })
        }
        localStorage.removeItem('products');
    }

    return (
        <div className="product-form__container">
        <h1>{value === "add" ? "Add New Product" : "Edit Product"}</h1>
        <form onSubmit={formik.handleSubmit} className="product-form">
            <TextField
                label="Product name"
                variant="outlined"
                name="title"
                id="title"
                value={formik.values.title}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.title && Boolean(formik.errors.title)}
                helperText={formik.touched.title && formik.errors.title}
                style={{width: "100%", color: "grey", margin: "12px 0px"}}
                color="primary"
            />
            <TextField
                label="Description"
                variant="outlined"
                name="description"
                id="description"
                value={formik.values.description}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.description && Boolean(formik.errors.description)}
                helperText={formik.touched.description && formik.errors.description}
                style={{width: "100%", color: "grey", margin: "12px 0px"}}
                color="primary"
            />
            <TextField
                label="Price"
                variant="outlined"
                type="number"
                name="price"
                id="price"
                value={formik.values.price}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.price && Boolean(formik.errors.price)}
                helperText={formik.touched.price && formik.errors.price}
                InputProps={{
                    inputProps: { 
                        min: 0 
                    },
                    startAdornment: <InputAdornment position="start">$</InputAdornment>
                }}
                style={{width: "100%", color: "grey", margin: "12px 0px"}}
                color="primary"
            />
            <TextField
                label="Image url"
                variant="outlined"
                name="imageUrl"
                id="imageUrl"
                value={formik.values.imageUrl}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.imageUrl && Boolean(formik.errors.imageUrl)}
                helperText={formik.touched.imageUrl && formik.errors.imageUrl}
                style={{width: "100%", color: "grey", margin: "12px 0px"}}
                color="primary"
            />
            <input type="submit" value={value === "add" ? "Add product" : "Edit Product"} className="authorization__button"/>
            <input type="button" value="Back" className="authorization__button authorization__button--grey" onClick={() => {history.push('/inventory')}}/>
        </form>
        </div>
    )
}
