import React from 'react'
import './Inventory.css'
import { ProductModification } from './ProductModification';
import { ProductForm } from './ProductForm';
import { useParams } from 'react-router-dom';

export const Inventory = () => {

    const {action, id} = useParams();

    return (
        <div className="inventory">
            <div className="inventory__header"><span className="inventory__header--span">Edit, Add, Remove</span> - modify your catalog now!</div>
            <div className="add-product">
            {action
            ?
                <ProductForm value={action} id={id}/>
            :
                <>
                <ProductModification title="Edit product" value="edit" icon={<i className="fas fa-pen"></i>}/>
                <ProductModification title="Add new product" value="add" icon={<i className="fas fa-plus"></i>}/>
                <ProductModification title="Remove product" value="remove" icon={<i class="fas fa-trash-alt"></i>}/>
                </>
            }
            </div>
        </div>
    )
}
