import React from 'react'
import './LandingPage.css'
import dropshipLogo from '../../icons/dropship-logo-transparent.png'
import { LandingButton } from './LandingButton';
import { useHistory } from 'react-router-dom';

export const LandingPage = () => {

    const history = useHistory();

    const landingButtons = [
        {
            title: "about"
        },
        {
            title: "catalog"
        },
        {
            title: "pricing"
        },
        {
            title: "suppliers"
        },
        {
            title: "help center"
        },
        {
            title: "blog"
        },
        {
            title: "sign up now",
            class: "outlined",
            route: "/register"
        },
        {
            title: "login",
            class: "login",
            route: "/login"
        },
    ];

    const handleButtonClick = (route) => {
        history.push(route);
    }

    return (
        <div className="landing__page">
            <div className="landing__image">
                <div className="landing__header">
                    <div className="landing-header__item landing-header__item--logo"><img src={dropshipLogo} alt="logo"/></div>
                    <div className="landing-header__item landing-header__item--buttons">
                        {
                            landingButtons.map(item => {
                                return <LandingButton title={item.title} buttonClass={item.class ? `landing__button--${item.class}` : "landing__button--normal"} handleClick={() => handleButtonClick(item.route)}/>
                            })
                        }
                    </div>
                </div>
                <div className="landing__banner">
                    <div className="landing-banner__item landing-banner__logo"><img src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/356Logo.svg" alt="dropship logo"/></div>
                    <h4 className="landing-banner__item landing-banner__text">WE GOT YOUR SUPPLY CHAIN COVERED</h4>
                    <h4 className="landing-banner__item landing-banner__text">365 DAYS A YEAR!</h4>
                </div>
                <button className="landing-banner__button" onClick={() => handleButtonClick("/register")}>Sign up here</button>
            </div>
        </div>
    )
}
