import React from 'react'
import './LandingButton.css';

export const LandingButton = ({title, buttonClass, handleClick}) => {
    return (
        <button className={`landing__button ${buttonClass}`} onClick={handleClick}>{title}</button>
    )
}
