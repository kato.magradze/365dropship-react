import React from 'react'
import './SignUp.css';
import { AuthorizationModal } from './AuthorizationModal';
import { useHistory, useLocation } from 'react-router-dom';
import { signUp } from '../../Services/authorizationServices';
import { logIn } from '../../Services/authorizationServices';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { checkToken } from '../../Utils/checkToken';

const signUpSchema = yup.object({
    firstName: yup
    .string()
    .required('Required'),
    lastName: yup
    .string()
    .required('Required'),
    email: yup
    .string()
    .email('Invalid email address')
    .required('Required'),
    password: yup
    .string()
    .required('Required')
    .min(4, 'Password must be at least 4 characters long'),
    passwordConfirmation: yup
    .string()
    .required('Required')
    .oneOf([yup.ref('password'), null], 'Passwords do not match')
})

const logInSchema = yup.object({
    email: yup
    .string()
    .email('Invalid email address')
    .required('Required'),
    password: yup
    .string()
    .required('Required')
    .min(4, 'Password must be at least 4 characters long')
})


export const Authorization = () => {

    const history = useHistory();

    const signUpFormik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            passwordConfirmation: ''
        },
        onSubmit: (values) => {
            signUp(values.firstName, values.lastName, values.email, values.password, values.passwordConfirmation).then(result => {
                checkToken(history);
            })
            .catch(err => alert("Registration Error"))
        },
        validationSchema: signUpSchema,
    });

    const logInFormik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        onSubmit: (values) => {
            logIn(values.email, values.password).then(result => {
                checkToken(history);
            })
            .catch(err => alert("Login Error"))
        },
        validationSchema: logInSchema,
    });

    const location = useLocation();

    return (
        <div className="authorization__page">
            <div className="authorization__image">
                { location.pathname === "/register" && 
                    <AuthorizationModal title="Sign Up" val="register" buttonText="Sign Up" formik={signUpFormik}/>
                }
                { location.pathname === "/login" && 
                    <AuthorizationModal title="Members Log In" val="login" buttonText="Log In" formik={logInFormik}/>
                }
            </div>
        </div>
    )
}
