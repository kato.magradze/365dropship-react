import React, {useState, useEffect} from 'react'
import './Cart.css'
import { TextField } from '@material-ui/core';
import noImage from '../../images/no_image_available.jpg'
import { Button } from '@material-ui/core';
import useStyles from '../../styles/UseStyles';
import { removeFromCart, getCart, addToCart } from '../../Services/authorizationServices';
import { useFormik } from 'formik';
import * as yup from 'yup';

export const CartRow = ({item, setCartData}) => {

    const classes = useStyles();

    const formik = useFormik({
        initialValues: {
            quantity: '',
        },
        onSubmit: (values) => {
            addToCart(item.id, values.quantity * -1).then(res => {
                alert(`${values.quantity} product${values.quantity > 1 ? "s have" : " has"} been removed from your cart.`);
                getCart().then(data => {
                    setCartData(data);
                }).catch(err => {
                    alert("Cart empty");
                });
                formik.setValues({quantity: ""})
            }).catch(err => {
                alert("Could not remove these items from cart. Try again.");
            })
        },
        validationSchema: yup.object({
            quantity: yup
            .number()
            .required('Required')
            .integer('Number must be an integer')
            .min(0, 'Number cannot be less than 0')
            .max(item.qty, 'Exceeds product quantity')
        }),
    });

    useEffect(() => {
        if(item.qty == 0){
            removeFromCart(item.id)
            getCart().then(data => {
                setCartData(data);
            }).catch(err => {
                alert("Cart empty");
            })
        }
    }, [item.qty])

    const handleRemoveFromCart = (id) => {
        removeFromCart(id).then(res => {
            alert(`${item.qty} product${item.qty > 1 ? "s have" : " has"} been removed from your cart`);
            getCart().then(data => {
                setCartData(data);
            }).catch(err => {
                alert("Cart empty");
            });
        }).catch(err => {
            alert("Could not remove item from cart");
        })
    }

    const imgError = (event) => {
        event.target.src = noImage;
    }

    return (
        <tr className="cart__row">
            <td className="cart__data cart__data--img"><img onError={imgError} alt="cart product" src={item.image} className="cart__img"/></td>
            <td className="cart__data cart__data--title">{item.title}</td>
            <td className="cart__data cart__data--quantity"> {item.qty} x</td>
            <td className="cart__data cart__data--remove-several">
                <form className="remove-several__wrapper" onSubmit={formik.handleSubmit}>
                <TextField
                    placeholder="Quantity to delete"
                    type="number"
                    name="quantity"
                    id="quantity"
                    variant="outlined"
                    value={formik.values.quantity}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.quantity && Boolean(formik.errors.quantity)}
                    helperText={formik.touched.quantity && formik.errors.quantity}
                    style={{width: '200px'}}
                    FormHelperTextProps={{ classes: { root: classes.helperText } }}
                />
                <button className="cart-remove-several__button" type="submit"><i className="fas fa-trash-alt remove-several__icon"></i></button>
                </form>
            </td>
            <td className="cart__data cart__data--remove-all">
                <Button className={classes.removeAll} onClick={() => handleRemoveFromCart(item.id)}>Remove all</Button>
            </td>
            <td className="cart__data cart__data--total">{item.qty * item.price}$</td>
        </tr>
                        
    )
}
