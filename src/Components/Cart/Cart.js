import React, { useEffect, useState } from 'react'
import './Cart.css'
import { getCart } from '../../Services/authorizationServices';
import { CartRow } from './CartRow';

export const Cart = () => {

    const [cartData, setCartData] = useState([]);

    useEffect(() => {
        getCart().then(data => {
            setCartData(data);
        }).catch(err => {
            alert("Empty cart");
        })
    }, []);

    return (
        <div className="cart">
            <div className="cart__total">Cart total: <span className="cart__total--span">${cartData.cartItem && cartData.cartItem.totalAmount}</span></div>
            <div className="cart__content">
                <table className="cart__table">
                    <tr className="cart__row cart__row--header">
                        <th className="cart__header cart__header--img">Product image</th>
                        <th className="cart__header">Title</th>
                        <th className="cart__header cart__header--quantity">Quantity</th>
                        <th className="cart__header cart__header--remove-several">Remove several</th>
                        <th className="cart__header">Remove all</th>
                        <th className="cart__header">Total</th>
                    </tr>
                    {cartData.cartItem && cartData.cartItem.items.map(item => {
                        return (
                            <CartRow item={item} setCartData={setCartData}/>
                        )
                    })}
                </table>
            </div>
        </div>
    )
}
