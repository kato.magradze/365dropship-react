import { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router';
import Catalog from './Catalog/Catalog';
import Sidebar from './SideBar/Sidebar';
import Modal from './Modal/Modal';
import './Main.css';

const Main = () => {
    const history = useHistory();

    const { category, id } = useParams();

    const [searchValue, setSearchValue] = useState("");
    const [categoryFilter, setCategoryFilter] = useState("all-categories");
    const [maxPrice, setMaxPrice] = useState(17000);
    // console.log("🚀 ~ file: Main.js ~ line 16 ~ Main ~ maxPrice", maxPrice)
    const [priceRangeValue, setPriceRangeValue] = useState([0, maxPrice])
    // console.log("🚀 ~ file: Main.js ~ line 18 ~ Main ~ priceRangeValue", priceRangeValue)
    const [profitRangeValue, setProfitRangeValue] = useState([0, 15000]);

    useEffect(() => {
        setCategoryFilter(category || "all-categories");
    }, [category])

    const handleOpen = (id) => {
        history.push(`/catalog/${categoryFilter}/${id}`);
    }

    const handleResetFilter = () => {
        setPriceRangeValue([0, maxPrice])
        setProfitRangeValue([0, 15000])
    }

    return (
        <main className="main">
            <Sidebar 
                setCategoryFilter={setCategoryFilter} 
                setSearchValue={setSearchValue}
                priceRangeValue={priceRangeValue} 
                setPriceRangeValue={setPriceRangeValue} 
                profitRangeValue={profitRangeValue} 
                setProfitRangeValue={setProfitRangeValue} 
                handleResetFilter={handleResetFilter}
                maxPrice={maxPrice}
            />
            <Catalog 
                handleOpen={handleOpen} 
                categoryFilter={categoryFilter} 
                searchValue={searchValue} 
                setSearchValue={setSearchValue} 
                priceRangeValue={priceRangeValue}
                setMaxPrice = {setMaxPrice}
                maxPrice={maxPrice}
                setPriceRangeValue={setPriceRangeValue}
                setProfitRangeValue={setProfitRangeValue}
            />
            {id && 
                <Modal categoryFilter={categoryFilter}/>
            }
        </main>
    );
}

export default Main;