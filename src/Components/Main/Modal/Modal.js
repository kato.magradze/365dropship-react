import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import BlueButton from '../Catalog/Header/BlueButton'
import './Modal.css'
import CircularProgress from '@material-ui/core/CircularProgress';
import { getSingleProduct } from '../../../Services/authorizationServices'
import noImage from '../../../images/no_image_available.jpg'
import { addToCart } from '../../../Services/authorizationServices';


const Modal = ({categoryFilter}) => {

    const history = useHistory();

    const { id } = useParams();

    const [modalProduct, setModalProduct] = useState({});
    const [productLoading, setProductLoading] = useState(true);

    useEffect(() => {
        id && fetchSingleProduct(parseInt(id))
    }, [id]);

    const fetchSingleProduct = (id) => {
        // getProducts(id).then(data => {
        //     setModalProduct(data);
        // }).finally(() => {
        //     setProductLoading(false);
        // })

        getSingleProduct(id).then(data => {
            setModalProduct(data);
        }).catch(err => {
            alert("??????")
        }).finally(() => {
            setProductLoading(false);
        })
    }

    const handleClose = (event) => {
        if(event.target !== event.currentTarget) return;
        setModalProduct();
        history.push(`/catalog/${categoryFilter}`);
    }

    const imgError = (event) => {
        event.target.src = `${noImage}`;
    }

    const handleAddToCart = () => {
        addToCart(id, 1).then(res => {
            alert("Product added to cart successfully")
        }).catch(err => {
            alert("Could not add product to cart. Try again.")
        })
    }

    if(productLoading) {
        return (<div className="modal__container">
                    <div className="modal__card model__card--loading"><CircularProgress size={80} color="primary"/></div>
                </div>)
    }
    return (
        <div className="modal__container" onClick={handleClose}>
            <div className="modal__card">
                <div className="modal__close" onClick={handleClose}>✖</div>
                <div className="modal__item">
                    <div className="modal-item__right">
                        <div className="item__info item__info--price">
                            <div className="price__item"><span className="span__item--styled">${modalProduct.price}</span> <br/>RRP</div>
                            <div className="price__item"><span className="span__item--styled">$6</span>  <br/>Cost</div>
                            <div className="price__item"><span className="span__item--styled span__item--blue">60% / 9$</span>  <br/>Profit</div>
                        </div>
                        <div className="item__info item__info--image">
                            <img onError={imgError} className="item__image" src={modalProduct.imageUrl} alt="product"/>
                        </div>
                    </div>
                    <div className="modal-item__left">
                        <div className="item__features item__features--details">
                            <div className="details__item details__item--code">SKU# bgb-s2412499</div>
                            <div className="details__item details__item--supplier">Supplier: <span className="supplier">SP-Supplier115</span></div>
                        </div>
                        <h1 className="item__features item__features--title">{modalProduct.title}</h1>
                        <div className="item__features item__features--inventory">
                            <BlueButton title="Add to My Inventory" handleClick={handleAddToCart} big/>
                        </div>
                        <h3 className="item__features item__features--productdesc">Product Description</h3>
                        <div className="item__features item__features--description">{modalProduct.description}</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Modal;
