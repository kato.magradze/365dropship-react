import React from 'react'
import './ShipOptions.css';

const ShipOptions = ({title, modifier}) => {
    return (
        <div className={`ship ship__${modifier}`}>
            <div className={`ship__item ship__item--title`}>{title}</div>
            <div className="ship__item ship__item--arrow"><i className="fas fa-angle-down"></i></div>
        </div>
    )
}

export default ShipOptions;
