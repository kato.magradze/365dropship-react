import React from 'react'
import './Range.css'

import Slider from '@material-ui/core/Slider';
import useStyles from '../../../styles/UseStyles';

const Range = ({title, id, rangeValue, setRangeValue, maxPrice}) => {

    const classes = useStyles();

    const handleChange = (event, newValue) => {
        setRangeValue(newValue);
    };

    return (
        <>
            <div className={id}>{title}</div>
            <div className={classes.root}>
                <Slider
                    color='secondary'
                    value={rangeValue}
                    onChange={handleChange}
                    min={0}
                    max={maxPrice}
                />
            </div>
            <div className="range-values">
                <div className="range-values__item">
                    <div className="value__item value__item--currency">$</div>
                    <div className="value__item value__item--price">{rangeValue[0]}</div>
                </div>
                <div className="range-values__item">
                    <div className="value__item value__item--currency">$</div>
                    <div className="value__item value__item--price">{rangeValue[1]}</div>
                </div>
            </div>
        </>
    )
}

export default Range;
