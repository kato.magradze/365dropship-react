import { useEffect, useState, useCallback } from 'react';
import Choose from './Choose';
import ShipOptions from './ShipOptions';
import BlueButton from '../Catalog/Header/BlueButton'
import './Sidebar.css';

import {getProducts} from './../../../Services/productServices.js'
import Range from './Range';


const Sidebar = ({setCategoryFilter, setSearchValue, priceRangeValue, setPriceRangeValue, profitRangeValue, setProfitRangeValue, handleResetFilter, maxPrice}) => {

    const [categories, setCategories] = useState([]);

    const fetchData = useCallback(async () => {
        let result = await getProducts('categories');
        localStorage.setItem('categories', JSON.stringify(result));
        setCategories(result);
    }, [setCategories]);

    useEffect(() => {
        const localCategories = localStorage.getItem('categories');
        if(localCategories) {
            setCategories(JSON.parse(localCategories));
        }
        else {
            fetchData();
        }
    }, [fetchData, setCategories]);


    return (
        <section className="aside__item aside__item--choose">
            <Choose title="Choose Niche" modifier="niche" categories={null}/>
            <Choose title="Choose Category" modifier="category" categories={categories} setCategoryFilter={setCategoryFilter} setSearchValue={setSearchValue}/>
            <ShipOptions title="Ship From" modifier="shipfrom"/>
            <ShipOptions title="Ship To" modifier="shipto"/>
            <ShipOptions title="Select Supplier" modifier="supplier"/>
            <Range title="Price Range" id="priceRange" rangeValue={priceRangeValue} setRangeValue={setPriceRangeValue} maxPrice={maxPrice}/>
            <Range title="Profit Range" id="profitRange" rangeValue={profitRangeValue} setRangeValue={setProfitRangeValue} maxPrice={15000}/>
            <BlueButton title="Reset filter" val="resetFilter" handleClick={handleResetFilter} medium/>
        </section>
    );
}

export default Sidebar;