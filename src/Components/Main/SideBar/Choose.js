import React, { useState } from 'react'
import { useHistory } from 'react-router'
import './Choose.css'

const Choose = ({title, modifier, categories, setCategoryFilter, setSearchValue}) => {

    const history = useHistory();

    const [currentCategory, setCurrentCategory] = useState("")

    const handleCategoryChange = (event) => {
        let categoryValue = event.target.attributes.value.value;
        setCategoryFilter(categoryValue);
        setCurrentCategory(categoryValue);
        setSearchValue("");
        history.push(`/catalog/${categoryValue}`);
    }

    const [menu, setMenu] = useState(false);

    const handleMenuClick = () => {
        setMenu(!menu);
    }

    return (
        <div className={`choose choose__${modifier}`}>
            <div className={`choose__item choose__item--${modifier} ${menu ? '' : 'choose__item--arrowdown'}`} onClick={handleMenuClick}>{title}</div>
            
                {categories && 
                    <ul className={`choose__list ${menu ? 'choose__list--visible' : 'choose__list--hidden'}`}>
                        <li className={`choose-list__item ${currentCategory === "all-categories" ? "choose-list__item--clicked" : ""}`} value="all-categories" onClick={handleCategoryChange}>...</li>
                        {categories.map(category => {
                            return <li className={`choose-list__item ${currentCategory === category ? "choose-list__item--clicked" : ""}`} key={category} value={category} onClick={handleCategoryChange}>{category}</li>
                        })}
                    </ul>
                }
        </div>
    )
}

export default Choose;
