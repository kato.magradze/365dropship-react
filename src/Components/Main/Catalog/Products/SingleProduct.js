import ProductHover from './ProductHover';
import './SingleProduct.css'
import { useState, useEffect} from 'react'
import noImage from '../../../../images/no_image_available.jpg'
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { Box } from '@material-ui/core';

const SingleProduct = ({id, image, title, price, handleCheck, handleOpen, productIds, setCartData}) => {

    const [checkboxValue, setCheckboxValue] = useState(false);
    const [src, setSrc] = useState(image);

    const [quantity, setQuantity] = useState(1);

    const checkboxClickedHandler = () => {
        handleCheck(id);
    }

    useEffect(() => { 
        const found = productIds && productIds.find(item => item === id);
        setCheckboxValue(found);
    }, [productIds, id]);

    const imgError = (event) => {
        setSrc(noImage);
    }

    const handleQuantityIncrement = (event) => {
        event.stopPropagation();
        setQuantity(prev => prev + 1);
    }

    const handleQuantityDecrement = (event) => {
        event.stopPropagation();
        setQuantity(prev => {
            if(prev > 0) return prev - 1;
            else return prev;
        });
    }

    return (
    <div className={`catalog__item catalog__item--product ${checkboxValue ? "catalog__item--highlighted" : ""}`}>
        <ProductHover id={id} checkboxClicked={checkboxClickedHandler} isClicked={checkboxValue} setCartData={setCartData} quantity={quantity} setQuantity={setQuantity}/>
        <div onClick={() => handleOpen(id)} className="catalog__item--container">
                <div className="product__item product__item--image">
                    <img onError={imgError} className="product__img" alt="product1" src={src} />
                </div>
                
                <div className="product__item product__item--title">
                    <h3 className="product__heading">{title}</h3>
                </div>
                <div className="product__item product__item--supplier product__item--title">
                    <div className="product__supplier">By: <span className="supplier__name">SP-Supplier115</span></div>
                    <Box p={2}>
                    <ButtonGroup color="primary">
                        <Button onClick={handleQuantityDecrement}>-</Button>
                        <Button disabled style={{color: "grey"}}>{quantity}</Button>
                        <Button onClick={handleQuantityIncrement}>+</Button>
                    </ButtonGroup>
                    </Box>
                </div>
            <div className="product__item product__item--price">
                <div className="price__item">
                    <div className="price">${price}</div>
                    <div className="rrp">RRP</div>
                </div>
                <div className="divider"></div>
                <div className="price__item">
                    <div className="price">$6</div>
                    <div className="rrp">Cost</div>
                </div>
                <div className="divider"></div>
                <div className="price__item">
                    <div className="price price__profit">60%/$9</div>
                    <div className="rrp">Profit</div>
                </div>
            </div>
        </div>
    </div>
    )
}

export default SingleProduct;