import SingleProduct from "./SingleProduct";
import "./Products.css";
import { Grid } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import useStyles from "../../../../styles/UseStyles";

const Products = ({handleCheck, handleOpen, products, productIds, loading}) => {

    const classes = useStyles();

    return (
        <>
        {loading ? 
            (<div className="product__item product__item--image product__item--loading">
                <CircularProgress size={80} color="primary"/>
            </div>)
            :
            (<Grid className={classes.container} container>
                {products.map(item => {
                    return (
                        <Grid item xs={12} sm={6} md={4} lg={3} key={item.id}>
                            <SingleProduct
                                id={item.id}
                                // image={item.image}
                                image={item.imageUrl}
                                title={item.title}
                                price={item.price}
                                description={item.description}
                                handleCheck={handleCheck}
                                handleOpen={handleOpen}
                                productIds={productIds}
                            />
                        </Grid>
                    )
                })}
            </Grid>)
        }
        </>
)}

export default Products;