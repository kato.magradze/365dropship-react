import BlueButton from '../Header/BlueButton'
import './ProductHover.css'
import { addToCart } from '../../../../Services/authorizationServices';
import { removeFromCart } from '../../../../Services/authorizationServices';
import { getCart } from '../../../../Services/authorizationServices';

const ProductHover = ({id, checkboxClicked, isClicked = false, quantity, setQuantity}) => {

    const handleAddToCart = (id, qty) => {
        setQuantity(1);
        addToCart(id, qty).then(alert("Product has been added to your cart."))
    }

    return (
        <>
            <div className={`product__item product__item--options ${isClicked && "product__item--clicked"}`}>
                <input 
                className="options__item options__item--checkbox"
                id={id} 
                type="checkbox"
                onChange={checkboxClicked}
                checked={isClicked}
                />
                <label htmlFor={id}></label>
            </div>
            <div className="product__item product__item--add">
                <BlueButton className="options__item options__item--button" title="Add to Inventory" handleClick={() => handleAddToCart(id, quantity)}/>
            </div>
        </>
    );
}

export default ProductHover;