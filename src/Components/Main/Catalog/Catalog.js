import Products from './Products/Products';
import Header from './Header/Header';
import Sort from './Sort/Sort';

import './Catalog.css';
import React , {useState,useEffect, useCallback, useMemo} from 'react';

import handleSortProducts from './../../../Utils/handleSortProducts.js'
import { useHistory } from 'react-router-dom';
import { getAllProductsList } from '../../../Services/authorizationServices';

const Catalog = ({handleOpen, categoryFilter, searchValue, setSearchValue, priceRangeValue, setMaxPrice, maxPrice, setPriceRangeValue}) => {

    const history = useHistory();

    const [productIds, setProductIds] = useState([]);
    const [filteredProducts, setFilteredProducts] = useState([]);
    const [sortState, setSortState] = useState("default");
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const token = localStorage.getItem('token');
        if(!token) {
            history.push("/login");
        }
    }, [JSON.parse(localStorage.hasOwnProperty('token'))]);

    const allProducts = useMemo(() => JSON.parse(localStorage.getItem('products')), [JSON.parse(localStorage.hasOwnProperty('products'))])

    const applyFilters = useCallback((searchValue) => {        
        if(allProducts){
            setLoading(false);
            let filtered = allProducts.filter(product => {
                return (product.title.toLowerCase().includes(searchValue.toLowerCase()) 
                // && ((categoryFilter !== "all-categories" ? product.category.toLowerCase() === categoryFilter.toLowerCase() : true)) 
                && (product.price >= priceRangeValue[0] && product.price <= priceRangeValue[1]));
            })
            setFilteredProducts(filtered);
            handleClearAll();
        }
    }, [allProducts, categoryFilter, priceRangeValue])

    useEffect(() => {
        const timer = setTimeout(() => {
            applyFilters(searchValue)
        }, 500);
        return () => { clearTimeout(timer) }
    }, [searchValue, maxPrice]);

    useEffect(() => {
        const timer = setTimeout(() => {
            applyFilters(searchValue)
        }, 100);
        return () => { clearTimeout(timer) }
    }, [categoryFilter, priceRangeValue, allProducts])

    useEffect(() => {
        if(allProducts) {
            const prices = allProducts.map(item => item.price);
            setMaxPrice(Math.max(...prices));
            setPriceRangeValue([0, Math.max(...prices)]);
        }
    }, [allProducts]);

    const sortedProducts = useMemo(() => handleSortProducts(filteredProducts, sortState), [filteredProducts, sortState]);

    const handleSelectAll = () => {
        setProductIds(filteredProducts.map(item => item.id));
    }

    const handleClearAll = () => {
        setProductIds([]);
    }

    const handleCheck = (id) => {
        const foundId = productIds.find(item => item === id);

        if(foundId) {
            const filteredArr = productIds.filter(item => item !== id);
            setProductIds(filteredArr);
        }
        else {
            setProductIds([...productIds, id]);
        }
    }

    const fetchData = useCallback(async () => {
        // let result = await getProducts();
        let result = await getAllProductsList();
        localStorage.setItem('products', JSON.stringify(result));
        setFilteredProducts(result);
    }, [setFilteredProducts]) 

    useEffect(() => {
        const localProducts = localStorage.getItem('products');
        if(localProducts) {
            setFilteredProducts(JSON.parse(localProducts));
        }
        else {
            fetchData();
        }
    }, [fetchData, setFilteredProducts]);

    return (
        <div className="catalog">
            <Header 
                selectedCount={productIds} 
                productsData={sortedProducts} 
                handleSelectAll={handleSelectAll} 
                handleClearAll={handleClearAll}
                setSearchValue={setSearchValue}
                searchValue={searchValue}
            />
            <Sort setSortState={setSortState}/>
            <Products 
                handleCheck={handleCheck} 
                handleOpen={handleOpen} 
                products={sortedProducts} 
                productIds={productIds} 
                loading={loading}
            />
        </div>
    )
}

export default Catalog;