import Button from '@material-ui/core/Button';
import useStyles from '../../../../styles/UseStyles';

const BlueButton = ({title, handleClick, big, medium, val=""}) => {

    const classes = useStyles()

    return (
        <Button 
            className={
            [classes.button, 
            !(medium || big) ? classes.smallButton : classes.mediumAndBig,
            val === "selectAllIcon" ? classes.selectIcon : "", 
            val === "selectAll" ? classes.selectAllButton : "", 
            val === "resetFilter" ? classes.resetButton : ""]
            .join(" ")} 
            variant="contained" 
            color="primary" 
            onClick={handleClick} 
            size={big ? 'large' : (medium ? 'medium' : 'small')}>
            {title}
        </Button> 
    );
}

export default BlueButton;