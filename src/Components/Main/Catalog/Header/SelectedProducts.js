import './SelectedProducts.css';

const SelectedProducts = ({selectedCount, productsCount}) => {
    return (
        <div className="select">
            <div className="select__divider"></div>
            <div className="select__products select__products--visible">{`selected ${selectedCount} out of ${productsCount} products`}</div>
            <div className="select__products select__products--shortened">{`${productsCount} products`}</div>
        </div>
    );
}

export default SelectedProducts;