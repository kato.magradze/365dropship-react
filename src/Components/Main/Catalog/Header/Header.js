import './Header.css';
import BlueButton from './BlueButton'
import SearchField from './SearchField';
import HelpButton from './HelpButton';
import SelectedProducts from './SelectedProducts';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import Hidden from '@material-ui/core/Hidden';
import { addToCart } from '../../../../Services/authorizationServices';

const Header = ({selectedCount, productsData, handleSelectAll, handleClearAll, setSearchValue, searchValue}) => {

    const handleAddToCart = () => {
        // selectedCount.map(id => {
        //     addToCart(id, 1).then(res => {
        //        console.log("Added item with id", id) 
        //     }).catch(err =>{
        //         alert("Could not add items to cart. Try again.")
        //     });
        // })
    }

    return (
        <div className="header__item header__item--functions">
            <div className="functions__item functions__item--select">
                <BlueButton title="Select All" val="selectAll" handleClick={handleSelectAll}  medium/>
                <SelectedProducts selectedCount={selectedCount.length} productsCount={productsData.length}/>
                {selectedCount.length > 0 && <BlueButton title="Clear Selected" val="clearSelected" handleClick={handleClearAll} medium/>}
                <BlueButton title={<CheckCircleIcon/>} val="selectAllIcon" handleClick={handleSelectAll} small/>
            </div>
            <div className="functions__item functions__item--search">
                <SearchField setSearchValue={setSearchValue} searchValue={searchValue}/>
                <Hidden xsDown><BlueButton title='Add to Inventory' val="addToInventory" handleClick={handleAddToCart} big/></Hidden>
                <Hidden smUp><BlueButton title='Add' val="add" big/></Hidden>
                <HelpButton/>
            </div>
        </div>
    );
}

export default Header;