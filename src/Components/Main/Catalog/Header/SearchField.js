import './SearchField.css'

const SearchField = ({ setSearchValue, searchValue }) => {

    const searchValueChanged = (event) => {
        setSearchValue(event.target.value);
    }

    return (
        <>
            <div className="search__item search__item--field">
                <input 
                className="search__field" 
                type="text" 
                id="searchQuery" 
                placeholder="search..."
                onFocus={(e) => e.target.placeholder = ''}
                value={searchValue}
                onChange={searchValueChanged}/>
            </div>
            <button className="search__item search__item--button" id="searchButton">
                <i className="fas fa-search"></i>
            </button>
        </>
    );
}

export default SearchField;