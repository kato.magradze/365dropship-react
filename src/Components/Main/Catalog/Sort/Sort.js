import './Sort.css'
import SortIcon from '@material-ui/icons/Sort';
import useStyles from '../../../../styles/UseStyles';

const Sort = ({setSortState}) => {

    const classes = useStyles();

    const handleSort = (event) => {
        setSortState(event.target.value);
    }

    return (
        <div className="header__item header__item--sort">
            <SortIcon className={classes.sortIcon} fontSize='small'/>
            <div className="sort__item sort__item--text">Sort By:</div>
            <select className="sort__item" id="sort" onChange={handleSort}>
                <option value="default">New Arrivals</option>
                <option value="asc">Price: Low to High</option>
                <option value="desc">Price: High to Low</option>
                <option value="alphabetic">Product name: A to Z</option>
                <option value="reverse-alphabetic">Product name: Z to A</option>
            </select>
        </div>
    )
}

export default Sort;