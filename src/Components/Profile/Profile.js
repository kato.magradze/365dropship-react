import React from 'react'
import { useHistory } from 'react-router-dom'
import BlueButton from '../Main/Catalog/Header/BlueButton'
import './Profile.css'

export const Profile = () => {

    const history = useHistory();

    const handleLogOut = () => {
        localStorage.clear();
        history.push('/login');
    }

    return (
        <div className="profile">
            <BlueButton title="Log out" handleClick={handleLogOut} big/>
        </div>
    )
}
