import React from 'react'
import { Snackbar } from '@material-ui/core'
import { Alert, AlertTitle } from '@material-ui/lab';
import useStyles from '../../styles/UseStyles';

export const Notification = ({notify, setNotify}) => {

    const classes = useStyles();

    return (
        <Snackbar
            open={notify.isOpen}
            autoHideDuration={3000}
            anchorOrigin={{vertical: 'top', horizontal: 'right'}}
        >
            <Alert severity={notify.type} className={classes.alert}>{notify.message}</Alert>
        </Snackbar>
    )
}
