import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
        padding: '5px 18px'
    },
    button: {
        color: "white",
        margin: "5px 10px",
        whiteSpace: 'nowrap',
        fontWeight: '600',
        flexShrink: '0'
    },
    smallButton: {
        textTransform: 'unset',
        fontWeight: '400'
    }, 
    mediumAndBig: {
        '@media (max-width:1366px)': {
            fontSize: '11px'
        },
        '@media (max-width: 1300px)': {
            fontSize: '10px'
        }
    }, 
    selectIcon: {
        display: 'none',
        fontSize: '16px',
        padding: '2px',
        maxWidth: '30px',
        '@media (max-width: 1055px)': {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        }
    }, 
    selectAllButton: {
        '@media (max-width: 1055px)': {
            display: 'none'
        }
    },
    resetButton: {
        width: '90%',
        margin: '25px 10px 10px',
        '@media (max-width: 1366px)': {
            fontSize: '13px'
        },
        '@media (max-width: 1300px)': {
            fontSize: '13px'
        }
    },
    container: {
        padding: 20,
        overflowY: 'scroll',
        height: 'calc(100vh - 122px)'
    },
    sortIcon: {
        color: '#B6B9CA',
        marginRight: '5px'
    },
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: "white",
        border: '2px solid #000',
        padding: "30px"
    },
    textField: {
       '&:hover': {
        //    border: '1px solid red'
        // backgroundColor: 'red'
        // color: 'rgba($red, 0.75) !important'
        // transition: 'none !important'
       } 
    },
    removeAll: {
        backgroundColor: 'lightgrey'
    },
    helperText: {
        position: 'absolute',
        top: '3.5rem'
    }
})

export default useStyles;