import './App.css';
import Aside from './Components/Aside/Aside';
import Main from './Components/Main/Main';
import {Switch, Route, Redirect, useLocation} from 'react-router-dom';
import { Dashboard } from './Components/Dashboard/Dashboard';
import { Inventory } from './Components/Inventory/Inventory';
import { Cart } from './Components/Cart/Cart';
import { Orders } from './Components/Orders/Orders';
import { Transactions } from './Components/Transactions/Transactions';
import { Stores } from './Components/Stores/Stores';
import { Profile } from './Components/Profile/Profile';
import { useState, useEffect } from 'react';
import { LandingPage } from './Components/LandingPage/LandingPage';
import { Authorization } from './Components/LandingPage/Authorization';

function App() {

  const location = useLocation();

  const [renderAside, setRenderAside] = useState(false);

  useEffect(() => {
    if(location.pathname === "/" || location.pathname === "/register" || location.pathname === "/login") {
      setRenderAside(false);
    }
    else {
      setRenderAside(true);
    }
  }, [location, renderAside])

  return (
    <div className="App">
      {renderAside && <Aside/>}
      <Switch>
        <Route exact path="/">
          <LandingPage/>
        </Route>
        <Route exact path="/catalog">
          <Redirect to="/catalog/all-categories"/>
        </Route>
        <Route path="/dashboard">
          <Dashboard/>
        </Route>
        <Route path="/inventory/:action?/:id?">
          <Inventory/>
        </Route>
        <Route path="/cart">
          <Cart/>
        </Route>
        <Route path="/orders">
          <Orders/>
        </Route>
        <Route path="/transactions">
          <Transactions/>
        </Route>
        <Route path="/stores">
          <Stores/>
        </Route>
        <Route path="/profile">
          <Profile/>
        </Route>
        <Route path="/register">
          <Authorization/>
        </Route>
        <Route path="/login">
          <Authorization/>
        </Route>
        <Route exact path="/catalog/:category?/:id?">
          <Main/>
        </Route>
      </Switch>
    </div>
  );
}

export default App;
