export const checkToken = (history) => {
    const token = localStorage.getItem('token');
    if(token) {
        history.push(`/catalog`);
    }
}