
const handleSortProducts = (filteredProducts, sortState) => {

    const filteredCopy = [...filteredProducts];

    if(sortState === "asc") {
        return filteredCopy.sort((a,b) => a.price - b.price);
    }
    else if(sortState === "desc") {
        return filteredCopy.sort((a,b) => b.price - a.price);
    }
    else if(sortState === "alphabetic") {
        return filteredCopy.sort((a,b) => a.title.localeCompare(b.title));
    }
    else if(sortState === "reverse-alphabetic") {
        return filteredCopy.sort((a,b) => b.title.localeCompare(a.title));
    }
    else {
        // return filteredCopy;
        return filteredCopy.sort((a,b) => a.id - b.id);
    }
}

export default handleSortProducts;