import axios from "axios";

const SERVER_URL = "http://18.185.148.165:3000/"
const SERVER_URL_V1 = SERVER_URL + 'api/v1/'

axios.interceptors.request.use((config) => {
    config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;
    return config;
})

export const signUp = async (firstName, lastName, email, password, passwordConfirmation) => {
    try {
        const res = await axios.post(SERVER_URL + "register", {firstName, lastName, email, password, passwordConfirmation});
        localStorage.setItem("user", JSON.stringify(res.data.data));
        localStorage.setItem("token", res.data.data.token);
    }
    catch(err) {
        throw new Error(err);
    }
}

export const logIn = async (email, password) => {
    try {
        const res = await axios.post(SERVER_URL + "login", {email, password});
        localStorage.setItem("user", JSON.stringify(res.data.data));
        localStorage.setItem("token", res.data.data.token);
    }
    catch(err) {
        throw new Error(err);
    }
}

export const getCart = async () => {
    try {
        const res = await axios.get(SERVER_URL_V1 + "cart");
        return res.data.data;
    } catch(err) {
            if(err.response.status === 401) {
                window.location.href="/login";
                // alert("something went wrong");
                console.log(err);
            }
            throw new Error(err);
    }
}

export const getAllProductsList = async () => {
    try {
        const res = await axios.get(SERVER_URL_V1 + 'products');
        return res.data.data
    } catch (err) {
        console.log(err);
        return []
    }
}

export const getSingleProduct = async (id) => {
    try {
        const res = await axios.get(SERVER_URL_V1 + `products/${id}`);
        return res.data.data
    } catch (err) {
        throw new Error(err);
    }
}

export const addToCart = async (productId, qty) => {
    try {
        const res = await axios.post(SERVER_URL_V1 + `cart/add`, {productId, qty});
        return res.data.data
    } catch (err) {
        console.log(err);
    }
}

export const removeFromCart = async (id) => {
    try {
        const res = await axios.post(SERVER_URL_V1 + `cart/remove/${id}`);
    }
    catch(err) {
        console.log(err);
    }
}

export const addProduct = async (data) => {
    try {
        const res = await axios.post(SERVER_URL_V1 + `products`, data);
        return res.data.data;
    }
    catch(err) {
        throw new Error(err);
    } 
} 

export const editProduct = async (id, data) => {
    try {
        const res = await axios.put(SERVER_URL_V1 + `products/${id}`, data);
        return res.data.data;
    }
    catch(err) {
        throw new Error(err);
    } 
} 

export const removeProduct = async (id) => {
    try {
        const res = await axios.delete(SERVER_URL_V1 + `products/${id}`);
        return res.data.data;
    }
    catch(err) {
        throw new Error(err);
    } 
} 