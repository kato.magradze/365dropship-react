import axios from 'axios'

const baseUrl = 'https://fakestoreapi.com/products'

export const getProducts = async (id="") => {
    try {
        const { data } = await axios.get(`${baseUrl}/${id}`)
        return data
    } catch (error) {
        console.log(error)
        return []
    }
}