import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from "react-router-dom"
import { createMuiTheme, ThemeProvider} from '@material-ui/core';

const dropshipTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#61D5DF'
    }, 
    secondary: {
      main: '#5D6B9F'
    }
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 700,
      md: 1500,
      lg: 1700
    }
  },
  typography: {
    fontFamily: 'Gilroy-medium'
  }
})

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <ThemeProvider theme={dropshipTheme}>
        <App />
      </ThemeProvider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
